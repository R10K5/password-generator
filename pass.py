from random import randint

def main():
    choice_fun = input("Использовать спецсимволы? Y/N: ").upper() == "N"
    size = int(input("Какая длинна у пароля: "))

    print(generate(size, choice_fun))


def generate(size, gen_nospec):
    result = ""
    spec = []

    if gen_nospec: 
        spec = [*range(33, 48), *range(58, 65), *range(91, 97), *range(123, 127)]

    for _ in range(size):
        random_dec_num = randint(33, 126)

        while random_dec_num in spec:
            random_dec_num = randint(33, 126)

        random_char = chr(random_dec_num)
        result += random_char
    
    return result

if __name__ == "__main__": 
    main()
